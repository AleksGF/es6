import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    const firstFighterTotalHealth = firstFighter.health;
    const secondFighterTotalHealth = secondFighter.health;
    let firstFighterCurrentHealth = firstFighter.health;
    let secondFighterCurrentHealth = secondFighter.health;
    let pressedKeys = new Set();
    let canPlayerOneMakeCriticalHit = true;
    let canPlayerTwoMakeCriticalHit = true;
    let winner = null;

    function fightProcess(event) {
      //double hit of PlayerOne
      if (
        pressedKeys.has(controls.PlayerOneCriticalHitCombination[0]) && 
        pressedKeys.has(controls.PlayerOneCriticalHitCombination[1]) &&
        pressedKeys.has(controls.PlayerOneCriticalHitCombination[2]) &&
        canPlayerOneMakeCriticalHit
      ) {
        secondFighterCurrentHealth = secondFighterCurrentHealth - (firstFighter.attack * 2);
        canPlayerOneMakeCriticalHit = false;
        setTimeout(() => {
          canPlayerOneMakeCriticalHit = true;
        }, 10000);
      }
      //double hit of PlayerTwo
      if (
        pressedKeys.has(controls.PlayerTwoCriticalHitCombination[0]) && 
        pressedKeys.has(controls.PlayerTwoCriticalHitCombination[1]) &&
        pressedKeys.has(controls.PlayerTwoCriticalHitCombination[2]) &&
        canPlayerTwoMakeCriticalHit
      ) {
        firstFighterCurrentHealth = firstFighterCurrentHealth - (secondFighter.attack * 2);
        canPlayerTwoMakeCriticalHit = false;
        setTimeout(() => {
          canPlayerTwoMakeCriticalHit = true;
        }, 10000);
      }
      //hit of PlayerOne
      if (
        pressedKeys.has(controls.PlayerOneAttack) &&
        !pressedKeys.has(controls.PlayerOneBlock) &&
        !pressedKeys.has(controls.PlayerTwoBlock)
      ) {
        secondFighterCurrentHealth = secondFighterCurrentHealth - getDamage(firstFighter, secondFighter);
      }
      //hit of PlayerTwo
      if (
        pressedKeys.has(controls.PlayerTwoAttack) &&
        !pressedKeys.has(controls.PlayerTwoBlock) &&
        !pressedKeys.has(controls.PlayerOneBlock)
      ) {
        firstFighterCurrentHealth = firstFighterCurrentHealth - getDamage(secondFighter, firstFighter);
      }
      
      pressedKeys.delete(event.code);

      //check current Health and winner
      if ( firstFighterCurrentHealth <= 0 && firstFighterCurrentHealth < secondFighterCurrentHealth) {
        firstFighterCurrentHealth = 0;
        winner = secondFighter;
      }
      if ( secondFighterCurrentHealth <= 0 && secondFighterCurrentHealth < firstFighterCurrentHealth) {
        secondFighterCurrentHealth = 0;
        winner = firstFighter;
      }

      //health indicator change
      const firstFighterHealthIndicator = document.getElementById('left-fighter-indicator');
      const secondFighterHealthIndicator = document.getElementById('right-fighter-indicator');
      let firstFighterHealthPercent = firstFighterCurrentHealth / firstFighterTotalHealth * 100;
      let secondFighterHealthPercent = secondFighterCurrentHealth / secondFighterTotalHealth * 100;
      
      firstFighterHealthIndicator.style.width = firstFighterHealthPercent + '%';
      secondFighterHealthIndicator.style.width = secondFighterHealthPercent + '%';

      if ( winner ) {
        resolve(winner);
      }
    }

    function addKey(event) {
      if(!event.repeat ) {
        pressedKeys.add(event.code);
      }

      document.addEventListener('keyup', fightProcess);  
    }

    document.addEventListener('keydown', addKey);
  });
}

export function getDamage(attacker, defender) {
  // return damage
  const damage = Math.max( 0, getHitPower(attacker) - getBlockPower(defender));
  
  return damage;  
}

export function getHitPower(fighter) {
  const attack = fighter.attack;
  const criticalHitChance = Math.random() + 1;
  const power = attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter) {
  const defense = fighter.defense;
  const dodgeChance = Math.random() + 1;
  const power = defense * dodgeChance;

  return power;
}
