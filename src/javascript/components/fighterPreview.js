import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  if (fighter) {
    const fighterImage = createFighterImage(fighter);
    const fighterInfoContainer = createElement({
      tagName: 'ul',
    });
    for ( let [key, value] of Object.entries(fighter) ) {
      if (key === '_id' || key === 'source') continue;
      
      const charName = key.charAt(0).toUpperCase() + key.substr(1);
      const elem = createElement({
        tagName: 'li',
      });
      elem.style.listStyleType = 'none';
      elem.innerHTML = `<em>${charName}:</em> <strong>${value}</strong>`;

      fighterInfoContainer.append(elem);
    }
    
    fighterElement.append(fighterImage, fighterInfoContainer);
  }
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { src: source };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    title: name,
    alt: name,
    attributes,
  });

  return imgElement;
}
