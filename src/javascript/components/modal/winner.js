import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {
  // call showModal function 
  //showModal ({ title, bodyElement, onClose })
  const title = `${fighter.name} win!`;
  const bodyElement = createElement({
    tagName: 'div',
  });
  const winnerImage = createFighterImage(fighter);
  bodyElement.append(winnerImage);
  const onClose = () => {window.location.reload();};
  showModal({ title, bodyElement, onClose });
}
